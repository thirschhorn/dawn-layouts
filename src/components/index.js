// Layouts

// Core
import Logo from './core/dawnLogo'
import MetaNav from './core/dawnMetaNav'
import PrimaryNav from './core/dawnPrimaryNav'
import TitleNav from './core/dawnTitleNav'
import TabNav from './core/dawnTabNav'
import Tab from './core/dawnTabNav/dawnTab'
import Input from './core/dawnInput'
import Button from './core/dawnButton'
import Section from './core/dawnSection'
import Form from './core/dawnForm'
import Textarea from './core/dawnTextarea'
import Select from './core/dawnSelect'
import SelectOptions from './core/dawnSelect/selectOptions'
import Modal from './core/dawnModal'
import Card from './core/dawnCard'
import Panel from './core/dawnPanel'
import Box from './core/dawnBox'
import Datepicker from './core/dawnDatepicker'
import Checkbox from './core/dawnCheckbox'
import Radio from './core/dawnRadio'
import InputTooltip from './core/dawnInputTooltip'
import Widget from './core/dawnWidget'
import Upload from './core/dawnUpload'
import Switch from './core/dawnSwitch'
import Slider from './core/dawnSlider'
import FieldGroup from './core/dawnFieldGroup'
import CardSelect from './core/dawnCardSelect'

export default {
  install (Vue) {
    Vue.component(Logo.name, Logo)
    Vue.component(MetaNav.name, MetaNav)
    Vue.component(PrimaryNav.name, PrimaryNav)
    Vue.component(TitleNav.name, TitleNav)
    Vue.component(TabNav.name, TabNav)
    Vue.component(Tab.name, Tab)
    Vue.component(Input.name, Input)
    Vue.component(Button.name, Button)
    Vue.component(Section.name, Section)
    Vue.component(Form.name, Form)
    Vue.component(Textarea.name, Textarea)
    Vue.component(Select.name, Select)
    Vue.component(SelectOptions.name, SelectOptions)
    Vue.component(Modal.name, Modal)
    Vue.component(Card.name, Card)
    Vue.component(Panel.name, Panel)
    Vue.component(Box.name, Box)
    Vue.component(Datepicker.name, Datepicker)
    Vue.component(Checkbox.name, Checkbox)
    Vue.component(Radio.name, Radio)
    Vue.component(InputTooltip.name, InputTooltip)
    Vue.component(Widget.name, Widget)
    Vue.component(Upload.name, Upload)
    Vue.component(Switch.name, Switch)
    Vue.component(Slider.name, Slider)
    Vue.component(FieldGroup.name, FieldGroup)
    Vue.component(CardSelect.name, CardSelect)
  }
}
