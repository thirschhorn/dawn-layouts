// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueHotKey from 'v-hotkey'
import Vddl from 'vddl'
import DawnComponents from '@/components'

// Libraries
import './assets/sass/font-awesome/fontawesome-pro-brands.scss'
import './assets/sass/font-awesome/fontawesome-pro-core.scss'
import './assets/sass/font-awesome/fontawesome-pro-light.scss'
import './assets/sass/font-awesome/fontawesome-pro-regular.scss'
import './assets/sass/font-awesome/fontawesome-pro-solid.scss'
import 'bootstrap/dist/css/bootstrap.css'
import './assets/sass/main.scss'

Vue.config.productionTip = false

Vue.use(VueHotKey)
Vue.use(Vddl)
Vue.use(DawnComponents)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  template: '<App/>',
  components: { App }
})
