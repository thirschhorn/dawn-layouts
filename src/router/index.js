import Vue from 'vue'
import Router from 'vue-router'
import { routerHistory, writeHistory } from 'vue-router-back-button'
import LogIn from '@/components/LogIn/index'
import Dashboard from '@/components/Dashboard/index'
import ProjectsIndex from '@/components/Projects/index'
import Projects from '@/components/Projects/projects'
import WebProjects from '@/components/Projects/web'
import MobileProjects from '@/components/Projects/mobile'
import NewProject from '@/components/Projects/new/index'
import Reports from '@/components/Reports/index'
import Team from '@/components/Team/index'
import Admin from '@/components/Admin/index'
import Company from '@/components/Admin/company'
import Settings from '@/components/Admin/settings'

import Docs from '@/components/Docs'
import Buttons from '@/components/Docs/Buttons/index'
import ColorGuidelines from '@/components/Docs/Colors/ColorGuidelines'
import ColorPalette from '@/components/Docs/Colors/ColorPalette'
import TextField from '@/components/Docs/Forms/TextField'
import NumericField from '@/components/Docs/Forms/NumericField'
import DatepickerField from '@/components/Docs/Forms/DatepickerField'
import SelectField from '@/components/Docs/Forms/SelectField'
import CheckboxField from '@/components/Docs/Forms/CheckboxField'
import RadioField from '@/components/Docs/Forms/RadioField'
import TextAreaField from '@/components/Docs/Forms/TextAreaField'
import FileUploadField from '@/components/Docs/Forms/FileUploadField'
import FormBuilding from '@/components/Docs/Forms/FormBuilding'

Vue.use(Router)
Vue.use(routerHistory)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: {name: 'Log In'}
    },
    {
      path: '/login',
      name: 'Log In',
      component: LogIn,
      meta: {authLayout: true, theme: 'default'}
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {appLayout: true, theme: 'default'}
    },
    {
      path: '/projects',
      name: 'Projects',
      component: ProjectsIndex,
      redirect: {name: 'Projects'},
      meta: {appLayout: true, theme: 'default'},
      children: [
        {
          path: 'all',
          name: 'Projects',
          component: Projects,
          meta: {appLayout: true, theme: 'default', parent: 'projects'}
        },
        {
          path: 'web',
          name: 'Web Projects',
          component: WebProjects,
          meta: {appLayout: true, theme: 'default', parent: 'projects'}
        },
        {
          path: 'mobile',
          name: 'Mobile Projects',
          component: MobileProjects,
          meta: {appLayout: true, theme: 'default', parent: 'projects'}
        },
        {
          path: 'new',
          name: 'New Project',
          component: NewProject,
          meta: {navlessLayout: true, theme: 'default', parent: 'projects'}
        }
      ]
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Reports,
      meta: {appLayout: true, theme: 'default'}
    },
    {
      path: '/team',
      name: 'Team',
      component: Team,
      meta: {appLayout: true, theme: 'default'}
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      meta: {appLayout: true, theme: 'apple'},
      children: [
        {
          path: '/admin/company',
          name: 'Company',
          component: Company,
          meta: {appLayout: true, theme: 'default', parent: '/admin'}
        },
        {
          path: '/admin/settings',
          name: 'Settings',
          component: Settings,
          meta: {appLayout: true, theme: 'default', parent: '/admin'}
        }
      ]
    },
    {
      path: '/docs',
      name: 'Docs',
      component: Docs,
      meta: {docsLayout: true, theme: 'default'},
      children: [
        {
          path: '/docs/buttons',
          name: 'Buttons',
          component: Buttons,
          meta: {docsLayout: true, theme: 'default', parent: '/docs'}
        },
        {
          path: '/docs/colors',
          name: 'Colors',
          redirect: {name: 'Color Guidelines'},
          meta: {docsLayout: true, theme: 'default', parent: '/docs'},
          children: [
            {
              path: '/docs/colors/color-guidelines',
              name: 'Color Guidelines',
              component: ColorGuidelines,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/colors'}
            },
            {
              path: '/docs/colors/color-palette',
              name: 'Color Palette',
              component: ColorPalette,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/colors'}
            }
          ]
        },
        {
          path: '/docs/forms',
          name: 'forms',
          redirect: {name: 'Text Field'},
          meta: {docsLayout: true, theme: 'default', parent: '/docs'},
          children: [
            {
              path: '/docs/text-field',
              name: 'Text Field',
              component: TextField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/numeric-field',
              name: 'Numeric Field',
              component: NumericField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/datepicker-field',
              name: 'Datepicker Field',
              component: DatepickerField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/select-field',
              name: 'Select Field',
              component: SelectField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/checkbox-field',
              name: 'CheckboxField',
              component: CheckboxField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/radio-field',
              name: 'RadioField',
              component: RadioField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/text-area-field',
              name: 'Text Area Field',
              component: TextAreaField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/file-upload-field',
              name: 'File Upload Field',
              component: FileUploadField,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            },
            {
              path: '/docs/forms/form-building',
              name: 'Form Building',
              component: FormBuilding,
              meta: {docsLayout: true, theme: 'default', parent: '/docs/forms'}
            }
          ]
        }
      ]
    }
  ]
})

router.afterEach(writeHistory)

export default router
